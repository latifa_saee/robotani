<!---
## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.
-->
## Installation

Make sure you have all the necessary applications (php,mysql,apache/nginx) to run this project. 

To start of with, you should clone this project. To be able to clone this project, you must be explicitly invited under gitlab by a user.

```
git clone git@gitlab.com:latifa_saee/robotani.git
```

Once you clone your project, you should be able to test it with 
```
composer update
php artisan serve.
```

If things do not go as expected, you should check if you have the environment (.env) file. 
If you don't have one, find and copy the  .env.example file in this repository.
```
cp robotani/.env.example robotani/.env

```

Your git should ignore the .env file by default. This means it wont be tracked by git.
This is recommended and should be ignored. 
Your gitignore configaration file is in .gitignore file.
Edit the `.env` file to make sure it matches your system.

Then try again
```
composer update
php artisan serve.
```

If it still doesn't work, 

Then, 

* Open the console and cd your project root directory
* Run composer install or php composer.phar install
* Run php artisan key:generate
* Run php artisan migrate
* Run php artisan db:seed to run seeders, if any.
* Run php artisan serve

This should fix the problems if php artisan serve does not work immediately. 
However, you should really just use `composer update` and `php artisan serve`. 
The Update will make sure that we are using the most update copy of laravel files. 
In should also change the  composer.lock file. 

In a production environment, use `composer install` to make sure it is exactly the same 
as the development environment according to what have been defined in `composer.lock`.

<!---
## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Please find the developers at rizalmohdnor@iium.edu.my, latifa and Sharifa

## License

Currently this is in MIT license. Might change later.
A short snippet describing the license (MIT, Apache, etc.)
-->
