    
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      
      <div class="container" style="font-size: 18px; color:white;">
       
        <b><a class="navbar-brand" href="index.html" style="text-shadow: transparent; font-size:25px;">SmartTech Remote Farming</a></b>
       
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
       
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto" style="color:white;">
            <li class="nav-item">
              <a style="color:white;" class="nav-link" href="{{ url('/') }}">Home</a>
            </li>

            <li class="nav-item dropdown">
              <a style="color:white;"  class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Quality
              </a>

               <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href="{{ url('qualityA') }}">Quality A</a>
                <a class="dropdown-item" href="{{ url('qualityB') }}">Quality B</a>
                <a class="dropdown-item" href="{{ url('qualityC') }}">Quality C</a>
              </div>

            </li>

            <li class="nav-item">
              <a style="color:white;"  class="nav-link" href="{{ url('products') }}">Shop Now</a>
            </li>

            <li class="nav-item">
              <a style="color:white;"  class="nav-link" href="{{ url('about') }}" >About</a>
            </li>

            <li class="nav-item">
              <a style="color:white;" class="nav-link" href="{{ url('help') }}">Help</a>
            </li>

            <li class="nav-item">
              <a style="color:white;" class="nav-link" href="{{ url('contact') }}">Contact</a>
            </li> 


            <li class="nav-item dropdown">
              <a style="color:white;" class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Account
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <a class="dropdown-item" href="{{ url('login') }}">Log In</a>
                <a class="dropdown-item" href="{{ url('register') }}">Register</a>
              </div>
                
            </li>
                
          </ul>

        </div>

      </div>
      
    </nav>

             
  