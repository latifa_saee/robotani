 <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
        
          <div class="carousel-item active" style="background-image: url('https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2013/11/08/101182633-vertical-farm.1910x1000.jpeg')">
            
          <div class="carousel-caption d-none d-md-block">
      
              <h1>Welcome To Melon Farming</h1>
              <p>The best melon products.</p>
            </div>
          </div>
      
          <!-- Slide Two - Set the background image for this slide in the line below -->
      
          <div class="carousel-item" style="background-image: url('http://aasarchitecture.com/wp-content/uploads/Vertical-Farming-Centre-by-Lusine-Baghdasaryan-Aram-Shahoyan-00.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h1>Welcome To Melon Farming</h1>
              <p>The best melon products.</p>
            </div>
          </div>
      
          <!-- Slide Three - Set the background image for this slide in the line below -->
      
          <div class="carousel-item" style="background-image: url('http://www.dietoflife.com/wp-content/uploads/2016/03/123-3.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h1>Welcome To Melon Farming</h1>
              <p>The best melon products.</p>
            </div>
          </div>
      
        </div>
      
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
   
   </div>

</header>  <!--end of header-->