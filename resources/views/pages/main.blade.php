@extends('master')

@section('content')



@section('header')

         @include('layouts.header')
         

@endsection


  <h1 class="my-4">Future Fruits</h1>

<div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Crenshaw Melon</h4>
            <div class="card-body">
              <p class="card-text">Curcumismelo is their Latin family’s name. It’s a hybrid type of melon with a sweet, juicy orange flesh. It’s ovoid in shape and greenish-yellow skin. This variety is very popular.</p>
            </div>
            <div class="card-footer">
              <a href="http://invorma.com/15-varieties-of-melons-with-pictures/" class="btn btn-primary">Learn More</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">High in Nutrition</h4>
            <div class="card-body">
              <p class="card-text">Although it doesn’t seem as though a honeydew melon and a Hubbard squash have much in common, the two belong to the same botanical family. Melons, squash, and cucumbers are members of the Cucurbitaceae, or gourd family; they all grow on vines.</p>
            </div>
            <div class="card-footer">
              <a href="http://www.berkeleywellness.com/healthy-eating/food/article/melons-high-nutrition" class="btn btn-primary">Learn More</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">How to Use Melons</h4>
            <div class="card-body">
              <p class="card-text">With the exception of watermelon and horned melon, preparation is the same for all melons. Simply cut the melon open and remove the seeds and strings. It can be served in many attractive ways: cut into halves, quarters, wedges, or cubes;</p>
            </div>
            <div class="card-footer">
              <a href="http://www.berkeleywellness.com/healthy-eating/food/article/melons-high-nutrition" class="btn btn-primary">Learn More</a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

   <h2>Portfolio Heading</h2>

  <div class="row">
       
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m1.jpg" style="width: 356px; height:280px;" alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Twice As Nice</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color: yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <p>A new melon that leaves no dounbt when to harvest.
                $5.00 one seed.</p>
              </p>
            
            </div>
          
          </div>
        
      </div>

      
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m2.jpg" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Sweet Melon</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p>The perfect one serving melon, Early to harvest        
                <td>$2.90 one seed.</p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>        


      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m3.jpg" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Muskateer Hybrid</h5>

              </h4>

              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p>The perfect one serving melon, Early to harvest.        
                <td>$3.00 one seed</p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>        


                
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m4.jpg" style="width: 356px; height:280px;" alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Cantaloupe, Burpee</h5>

              </h4>
              
              <p class="card-text">
                 <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <P>Our classic hybrid melon with a well-deserved reputation for outstanding flavor.        
                <td>$4.00 one seed</p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>

      
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m5.jpg" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Vedrantais</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p>Italian heirloom with sweet, frarant orange flesh.
                <td>$4.00 one seed </p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>        


      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m6.jpg" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Cantaloupe, Burpee</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <P>Our classic hybrid melon with a well-deserved reputation for outstanding flavor.        
                <td>$4.00 one seed</p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>        



  </div><!--end of row-->


@endsection
