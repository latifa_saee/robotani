@extends('master')

@section('content')


<div class="col-md-5"><br><br>

     @if(count ($errors)>0)
               
                @foreach($errors->all() as $error)

                  <p class=" alert alert-danger">{{ $error}}</p>
                  
                @endforeach

         @endif 
         
    <div class="form-area">  
    
        <form  method ="post" action = "{{route('contactus')}}">
           
           {{ csrf_field() }}
    
          <br style="clear:both">
    
                    <h3 style="margin-bottom: 25px; text-align: center;">Contact Us</h3>
    
    				<div class="form-group">
	
    					<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
	
    				</div>

					<div class="form-group">
	
    					<input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
					
                    </div>

					<div class="form-group">
					
                    	<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
					
                    </div>


					<div class="form-group">
					
                    	<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
				
                	</div>

                    <div class="form-group">
                
                    <textarea class="form-control" type="textarea" id="message"  name = "message" placeholder="Message" maxlength="140" rows="7"></textarea>
                
                        {{-- <span class="help-block"><p id="characterLeft" class="help-block ">You have  reached the limit</p></span>    --}}                 
                    </div>
            
              <button type="submit" class="btn btn-primary pull-right">Submit Form</button><br><br>
        
        </form>

    </div>
    
  </div>


@endsection