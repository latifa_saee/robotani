@extends('master')

@section('content')

  <br><br><br>

  <ol class="breadcrumb">
       <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Products</li>
   </ol>

    <h3 class="my-4" >Most Recent Products</h3>

  <div class="row">
        
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="img-thumbnail" src="http://www.burpee.com/dw/image/v2/ABAQ_PRD/on/demandware.static/-/Sites-masterCatalog_Burpee/default/dw58ec5358/Images/Product%20Images/prod003510/prod003510.jpg?sw=322&sh=380&sm=fit" style="width: 356px; height:280px;" alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Twice As Nice</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color: yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <p style ="color:black;">A new melon that leaves no dounbt when to harvest.</p>
                <p>$5.00 one seed.</p>
                <button type="submit" class="btn btn-primary">Add To Cart</button> 
              </p>
            
            </div>
          
          </div> 

          </div>   

      
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="img-thumbnail" src="http://www.burpee.com/dw/image/v2/ABAQ_PRD/on/demandware.static/-/Sites-masterCatalog_Burpee/default/dwc6da35db/Images/Product%20Images/prod003506/prod003506.jpg?sw=265&sh=312&sm=fit" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Sweet Melon</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p style ="color:black;">The perfect one serving melon, Early to harvest.</p>       
                <p>$2.90 one seed.</p>
                 <button type="submit" class="btn btn-primary">Add To Cart</button>
              </p>
            
            </div>
          
          </div>
        
      </div>        
    

      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="img-thumbnail" src="http://www.burpee.com/dw/image/v2/ABAQ_PRD/on/demandware.static/-/Sites-masterCatalog_Burpee/default/dw71a0a114/Images/Product%20Images/prod000541/prod000541_alt1.jpg?sw=322&sh=380&sm=fit" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Muskateer Hybrid</h5>

              </h4>

              <p class="card-text">
               <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p style ="color:black;">The perfect one serving melon, Early to harvest.</p>      
                <p>$3.00 one seed</p>
                <button type="submit" class="btn btn-primary">Add To Cart</button>
              </p>
            
            </div>
          
          </div>
        
      </div>        


                
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="img-thumbnail" src="http://www.burpee.com/dw/image/v2/ABAQ_PRD/on/demandware.static/-/Sites-masterCatalog_Burpee/default/dwa535c2c9/Images/Product%20Images/prod000542/prod000542.jpg?sw=265&sh=312&sm=fit" style="width: 356px; height:280px;" alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Cantaloupe, Burpee</h5>

              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <P style ="color:black;">Our classic hybrid melon with a well-deserved reputation for outstanding flavor. </P>       
                <p>$4.00 one seed</p>
                <button type="submit" class="btn btn-primary">Add To Cart</button>
              </p>
            
            </div>
          
          </div>
        
      </div>
      
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="img-thumbnail" src="http://www.burpee.com/dw/image/v2/ABAQ_PRD/on/demandware.static/-/Sites-masterCatalog_Burpee/default/dw2fd24c48/Images/Product%20Images/prod003438/prod003438.jpg?sw=322&sh=380&sm=fit" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Vedrantais</h5>
              </h4>
              
              <p class="card-text">
               <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p style ="color:black;">Italian heirloom with sweet, frarant orange flesh.</p> 
               <p> $4.00 one seed </p> 
               <button type="submit" class="btn btn-primary">Add To Cart</button>   
              </p>
            
            </div>
          
          </div>
        
      </div>        


      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="img-thumbnail" src="http://www.burpee.com/dw/image/v2/ABAQ_PRD/on/demandware.static/-/Sites-masterCatalog_Burpee/default/dwd7ea6cdb/Images/Product%20Images/prod001943/prod001943.jpg?sw=265&sh=312&sm=fit" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5 >Cantaloupe, Burpee</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <P style ="color:black;">Our classic hybrid melon with a well-deserved reputation for outstanding flavor. </P>       
                <p>$4.00 one seed</p>
                 <button type="submit" class="btn btn-primary">Add To Cart</button>
              </p>
            
            </div>
          
          </div>
        
      </div>        
  </div><!--end of row-->


<br>
@endsection