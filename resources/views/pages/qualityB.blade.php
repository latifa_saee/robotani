@extends('master')


@section('content')


<!-- Page Content -->
    <div class="container"><br><br><br>

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Quality B
        <small>Subheading</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Quality B</li>
      </ol>

      <!-- Project One -->
      <div class="row">
                  
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m4.jpg" style="width: 356px; height:280px;" alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Cantaloupe, Burpee</h5>

              </h4>
              
              <p class="card-text">
                 <i class="material-icons" style="color: yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <P>Our classic hybrid melon with a well-deserved reputation for outstanding flavor.        
                <td>$4.00 one seed</p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>

      
      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m5.jpg" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Melon, Vedrantais</h5>
              </h4>
              
              <p class="card-text">
             <i class="material-icons" style="color: yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <p>Italian heirloom with sweet, frarant orange flesh.
                <td>$4.00 one seed </p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>        


      <div class="col-lg-4 col-sm-6 portfolio-item">
          
          <div class="card h-100">
            
            <a href="#"><img class="card-img-top" src="/images/m6.jpg" style="width: 356px; height:280px;"  alt=""></a>
            
            <div class="card-body">
             
              <h4 class="card-title">
                
                <h5>Cantaloupe, Burpee</h5>
              </h4>
              
              <p class="card-text">
                <i class="material-icons" style="color: yellow;">star</i>
                <i class="material-icons" style="color:yellow;">star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:yellow;" >star</i>
                <i class="material-icons" style="color:gray;">star</i>
                <P>Our classic hybrid melon with a well-deserved reputation for outstanding flavor.        
                <td>$4.00 one seed</p></td>
              </p>
            
            </div>
          
          </div>
        
      </div>        


  </div><!--end of row-->

      </div>
      <!-- /.row -->

      <hr>

      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>

    </div>
    <!-- /.container -->


@endsection