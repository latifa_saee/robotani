
@extends('master')



@section('content')


 <hr><h1> Essential Mineral Elements </h1><br><br><br>


      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
          <p class="float-right d-md-none">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
          </p>

        
         
        <br><br>
       
        <div class="row">
       
        <table>
        <tr><td style="padding-right: 20px;">
      
      <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Place
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">Gombak</a>
          <a class="dropdown-item" href="#">Putrajaya</a>
          <a class="dropdown-item" href="#">Petalingjaya</a>
        </div>
      </div>
      <input type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="Select a place">
    </div>
    </td>
    <br>
    <td> <nbsps>
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Batch Number
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">1</a>
          <a class="dropdown-item" href="#">2</a>
          <a class="dropdown-item" href="#">3</a>
          <a class="dropdown-item" href="#">4</a>
          <a class="dropdown-item" href="#">5</a>          
        </div>
      </div>
      <input type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="Select a batch number">
    </div> 
     </td>
    </tr>
    </table>


<table class="table">
  <thead>
    <tr>
      <th>2</th>
      <th>Macronutrients</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Carbon
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="carbon" placeholder="% of Carbon here"></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Hydrogen
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="hydrogen" placeholder="% of Hydrogen here"></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Oxygen
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="oxygen" placeholder="% of Oxygen here"></td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Nitrogen
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="nitrogen" placeholder="% of Nitrogen here"></td>
    </tr>
    <tr>
      <th scope="row">5</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Phosphoruse
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="phosphoruse" placeholder="% of Phosphoruse here"></td>
    </tr>
    <tr>
      <th scope="row">6</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Sulphur
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="sulphur" placeholder="% of Sulphur here"></td>
    </tr>
    <tr>
      <th scope="row">7</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Potassium
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="potassium" placeholder="% of Potassium here"></td>
    </tr>
    <tr>
      <th scope="row">8</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Calsium
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="calsium" placeholder="% of Calsium here"></td>
    </tr>
    <tr>
      <th scope="row">9</th>
      <td><label class="form-check-label">
    <input class="form-check-input" type="checkbox" value="">
    Magnasium
  </label></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="magnasium" placeholder="% of Magnasium here"></td>
    </tr>
    <tr>
      <td><button type="button" class="btn btn-secondary">+</button></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="micro-ut-water" placeholder="% of Water here"></td>
      <td><button type="button" class="btn btn-secondary">-</button> 
      <button type="button" class="btn btn-secondary">Add</button> </td>
    </tr>
    <tr>
      <td>
    <br>
    <button type="button" class="btn btn-secondary">Check</button> 
    <button type="button" class="btn btn-secondary" style="clear:both;">Cancel</button><hr>  
    </td>
    </tr>
  </tbody>
</table>

        </div><!--/row-->
        </div><!--/span-->

        <div class="col-8 col-md-3 sidebar-offcanvas" id="sidebar">
          <div class="list-group">
            <a href="#" class="list-group-item active">Quality A</a>
            <a href="#" class="list-group-item">Quality B</a>
            <a href="#" class="list-group-item">Quality C</a>
            <a href="#" class="list-group-item">Tastes</a>
            <a href="#" class="list-group-item">Color</a>
            <a href="#" class="list-group-item">Size</a>
          </div>
        </div><!--/span-->
      </div><!--/row-->

      <hr>
    
    <button type="button" class="btn btn-secondary" name="back">Back</button>
    <button type="button" class="btn btn-secondary" name="next">Next</button><hr>
   


   @endsection