@extends('master')



@section('content')



      <br><br><br><div class="row row-offcanvas row-offcanvas-right"> 
        
        <div class="col-12 col-md-9">
       
        
          <p class="float-right d-md-none">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
          </p>

           <div class="jumbotron">
             <ul>
               <li>
                 Select Micronutrients to feed the fruits
               </li>
             </ul>
            <img src="https://porcelainfacespa.com/blog/wp-content/uploads/2014/05/ph-scasle.jpg" alt="">
       
          </div>
   

  <table class="table">

  <tbody>
    <tr>    
  <td>
      <div class="input-group">
        <div class="input-group-btn">
         <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Select Place
         </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">Goombak</a>
          <a class="dropdown-item" href="#">Putra Jaya</a>
          <a class="dropdown-item" href="#">Petaling Jaya</a>
        </div>
   
      
      {{-- <input type="text" class="form-control" style="width:200px;" aria-label="Text input with dropdown button" placeholder="Select a place"> --}}
    
    </div>

</div>

    </td>



    <td>
        
     <div class="input-group"> 
      <div class="input-group-btn">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Select Batch Number
        </button>
        <div class="dropdown-menu">
          <a class="<dropdown-item " href="#">1</a>
          <a class="dropdown-item" href="#">2</a>
          <a class="dropdown-item" href="#">3</a>
          <div role="separator" class="dropdown-divider"></div>
        </div>
     
      {{-- <input type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="Select batch number"> --}}
     </div> 
   </td>
   
  </div>

   
   </tr>

    <tr>
      
      <td><button type="button" class="btn btn-primary">Check</button>
          <button type="button" class="btn btn-primary">Cancel</button></td>
      <td><input type="text" class="form-control" aria-label="Text input with dropdown button" name="ph-result" placeholder="pH % Result Here"></td>
    </tr>



    <tr>
     
      <td><br><br><br>
        <p>Edit the percentage of Lime and Sulfer to balance pH range.</p>
          <p><h3>Lime</h3><button type="button" class="btn btn-primary">+</button>
              <button type="button" class="btn btn-primary">-</button>
              <button type="button" class="btn btn-primary">Add</button>
              <input type="text" class="form-control" aria-label="Text input with dropdown button" name="ph-result" placeholder="50">

              </p>
              <p>Add lime to make water more alkaline.</p>
      </td>

       <td><br><br><br><br>
          <p><h3>Sulfer</h3><button type="button" class="btn btn-primary">+</button>
              <button type="button" class="btn btn-primary">-</button>
              <button type="button" class="btn btn-primary">Add</button>
              <input type="text" class="form-control" aria-label="Text input with dropdown button" name="ph-result" placeholder="50">

              </p>
              <p>Add sulfer to make water more acidic..</p>
      </td>
    </tr>

</tbody>
</table>

</div>
</div>

@endsection