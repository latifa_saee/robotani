<?php



Route::get('/', function () {   
    return view('pages.main');
});



// Route::get('/productQuality', function () {   
//     return view('pages.main');
// });


Route::get('/qualityA', function () {
    return view('pages.qualityA');
});

Route::get('/qualityB', function () {
    return view('pages.qualityB');
});

Route::get('/qualityC', function () {
    return view('pages.qualityC');
});



Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/help', function () {
    return view('pages.help');
});
 
Route::get('/products', function () {
    return view('pages.products');
});



Route::get('/organicmatters', function () {
    return view('pages.organicmatters');
});


Route::get('/size', function () {
    return view('pages.size');
});

Route::get('/spaceControl', function () {
    return view('pages.spaceControl');
});

Route::get('/tasteControl', function () {
    return view('pages.tasteControl');
});

Route::get('/air', function () {
    return view('pages.air');
});




Route::get('/ph', function () {
    return view('pages.ph-level');
});


// ============================================================
// ==========end of pages======================================




Auth::routes();
 

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');


Route::prefix('admin')->group(function(){

      
      Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');  

      Route::post('/login','Auth\AdminLoginController@Login')->name('admin.login.sumbit');

      Route::get('/', 'AdminController@index')->name('admin.dashboard');

      Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');


      Route::post('/password/email','Auth\AdminForgotPasswordController@sendResetLinkEmail')->name ('admin.password.email');

      Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name ('admin.password.request');

      Route::post('/password/reset','Auth\AdminResetPasswordController@reset');

      Route:: get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('password.reset');
});





Route::get('/register','Auth\RegisterController@showRegisterForm')->name('register');

Route::post('/register','Auth\RegisterController@create'); 


Route::get('/nutrient', 'NutrientController@show'); 
Route::post('/nutrient', 'NutrientController@addNutrients')->name('nutrient');



Route::get('contact', 'ContactUsController@getContactus');
Route::post('contact', 'ContactUsController@postContactus')->name('contactus');




Route::get('/my-chart', 'ChartController@index');






// Admin route Sharifa Part

Route::get('/admin', function () {
    return view('admin.admin-index');
});


Route::get('/index', function(){
    return view('admin.index');

});

























































// Route::group(['middleware'=>'auth'], function(){

//     Route::get('/home', function(){
    
//         return view('home');
    
//     })->name('home');

//     Route::get('/dashboard', function(){
    
//         return view('admin.home');
    
//     })->name('dashboard');


    // Route::get('/regulator', function(){
    
    //     return view('regulator');
    
    // })->name('regulator');

// });






// Route::get('/register','AuthController@showRegisterForm')->name('register'); 

// Route::post('/register', 'AuthController@register');


// Route::get('/admin', function () {
//     return view('admin.index');
// });



