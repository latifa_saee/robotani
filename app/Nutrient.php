<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nutrient extends Model
{
      protected $fillable = ['iron', 'Manganese','Copper', 'Molybdenum', 'Zinc', 'Boron', 'Nickel','Chlorine-Nickel', 'Category','batch', 'Chlorine-Nickel'];
}
