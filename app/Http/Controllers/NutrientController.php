<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Nutrient;




class NutrientController extends Controller
{
    public function show()
    {
    	return view('pages.nutrient');
    }


    public function addNutrients(Request $request)
    {
          
        $this->validate(request(),[

         
           'iron'=>'required',
            
            'Manganese'=>'required',

           'Copper'=> 'required',
           
            'Molybdenum'=> 'required',
           
            'Zinc'=> 'required',
           
            'Boron'=> 'required',
           
            'Nickel'=> 'required',

            'Chlorine-Nickel'=>'required',
           
            'Category'=> 'required',
           
            'batch'=> 'required'


    ]);

             
           $nutrienta= Nutrient::create([        
	       
           'iron'=>request('iron'),

           'Manganese'=> request('Manganese'),
           
           'Copper'=> request('Copper'),
           
            'Molybdenum'=> request('Molybdenum'),
           
            'Zinc'=> request('Zinc'),
           
            'Boron'=> request('Boron'),
           
            'Nickel'=> request('Nickel'),

            'Chlorine-Nickel'=>request('Chlorine-Nickel'),
           
            'Category'=> request('Category'),
           
            'batch'=> request('batch')


    	]);

        
       $nutrient = Nutirent::create($request->all());



        return redirect('/home')->with('status', 'nutrient added successfully!');
        
    }



         
}
