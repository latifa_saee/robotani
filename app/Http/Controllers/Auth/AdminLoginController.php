<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;


class AdminLoginController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest:admin', ['except' => ['logout']]);

	}


    public function showLoginForm()
    {
    	return view('auth.admin-login');
    }



    public function Login(Request $request)
    {
    	//validate the form data 
    	
         $this->validate($request,[

          'email'=>'required|email',
          'password'=>'required|min:6'

         ]);
    	
    	//Attempt to log the user in to their page
    	
    	if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password], $request->remember)){

    		//if, successful, then redirect to their intended lcoation

    		return redirect()->intended(route('admin.dashboard'))->with('status',' Logged in as an Admin Sccussfully!');  


    	}

        //if, unsuccessful, then redirect back login with the form data
        
        return redirect()->back()->withInput($request->only('email', 'remember'));
        
    	 
    }


    public function logout()
    {
       Auth::guard('admin')->logout();

        return redirect('/home');
    }

}
