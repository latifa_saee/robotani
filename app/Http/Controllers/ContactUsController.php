<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;

use Validator;

use Redirect;



class ContactUsController extends Controller
{
     
    public function getContactus()
    {
        return view('pages.contact');
    }

    public function postContactus(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required',
            'subject' => 'required',
        ]);

        // Write here your database logic
         
         $user_request= Contact::create([

        'name'  => request('name'),
        'email' => request('email'), 
        'mobile'=> request('mobile'),
        'subject'=>request('subject'),
          
          ]);

        \Session::put('success', 'Youe Request Submited Successfully...!!');
        
        return redirect('/')->with('status', 'scuccess');
    }


    
}

