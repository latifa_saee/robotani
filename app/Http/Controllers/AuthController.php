<!-- 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Auth;


class AuthController extends Controller

{
 
 // /*    Register new user      */ 
   
    public function showRegisterForm()
   
    {
    	return view('pages.register');
    }


    public function register(Request $request)
    
    {
    
    	$this->validate(request(),[

         'name'=>'required',
         'email'=>'required|email',
         'password'=>'required|confirmed'

   	]);

    	 $user= User::create([

        'name'  => request('name'),
        'email' => request('email'), 
        'password'=> bcrypt(request('password'))
          
          ]);

    	return redirect('/home')->with('status', 'You are registered!');
    	
    }


   
    /*     Login validation */
//==========================================//



    public function showLoginForm()

    {
    	return view('pages.login');
    }




    public function login(Request $request)
    
    {
    	

         $this->validate($request, [
        
        'email' => 'required|email|max:255',

        'password' => 'required|max:255',
     
        ]);

        
        if(Auth::attempt([
         	
         	'email'=>$request->email,

            'password'=>$request->password
        ])) 

        {

          $user = User::where('email', $request->email)->first();

          if($user->is_admin())
          
          {
            return redirect()->route('dashboard');
            // return redirect('/')->with('status', 'You are registered!');

          }
         //  else if($user->is_admin()== 2)
        
         //  {

         //  return redirect()->route('regulator');
         // }


         return redirect()->route('home');  

          
        	
        }

        return redirect()->back();
    	
    }





    public function validation($request)

   {

    return $this->validate($request, [
        
        'name' => 'required|max:255',
        'email' => 'required|email|unique:users|max:255',
        'password' => 'required|confirmed|max:255',
     
     ]);

   
   }


}

 -->